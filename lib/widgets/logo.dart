import 'package:flutter/material.dart';

class Logo extends StatefulWidget {
 
  @override
  _LogoState createState() => _LogoState();
}

class _LogoState extends State<Logo> {
  @override
  Widget build(BuildContext context) {
    return Padding(
            padding: const EdgeInsets.only(top:45,left: 137,bottom: 10,right: 136),
            child: Container(
                height: MediaQuery.of(context).size.height*.3,
      width:MediaQuery.of(context).size.width*.3,
              child: Image.asset('assets/logo.png'),

            ),
          );
  }
}