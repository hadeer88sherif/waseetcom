import 'package:flutter/material.dart';

class BtnBuilder extends StatelessWidget {
  final String text;
  BtnBuilder({this.text});
  @override
  Widget build(BuildContext context) {
    return  Padding(
              padding: const EdgeInsets.only(left: 20,right: 20,bottom: 20,top: 20),
              child: Container(
                height: MediaQuery.of(context).size.height * .06,
                decoration: BoxDecoration(color: Color(0xFF55C2D8),
                borderRadius: BorderRadius.circular(10)
                ),
                child: Center(child: Text(text,style: TextStyle(color: Color(0xFFFFFFFF),fontSize: 16),)),
              ),
            );
  }
}