import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class TextForm extends StatelessWidget {
  TextForm(
      {this.controller,
      this.hinttext,
      this.maxNum,
        this.height,
        this.maxLine,
      this.onsaved,
      this.prefixicon,
      this.suffixicon,
      this.suffixiconcolor,
      this.suffixiconfunction,
      this.color,
      this.width,
      this.textInputType});

  final TextEditingController controller;
  final int maxNum;
  final String hinttext;
  final IconData prefixicon;
  final IconData suffixicon;
  final Color color;
  final double height;
  final Color suffixiconcolor;
  final Function suffixiconfunction;
  final TextInputType textInputType;
  final double width;
  final Function onsaved;
  final int maxLine;

  @override
  Widget build(BuildContext context) {
    return Container(
      height:height,
      width: width,
      decoration: BoxDecoration(
          color: color,
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: TextFormField(
          textDirection: TextDirection.rtl,
          cursorColor: Colors.white,
          keyboardType: textInputType,
          maxLength: maxNum,
          maxLines: maxLine,
          onSaved: onsaved,
          controller: controller,
          decoration: InputDecoration(
            hintStyle: TextStyle(color: Color(0xFFB4B4B4),),
            hintText: hinttext,
            prefixIcon: Icon(prefixicon),
            suffixIcon: IconButton(
              icon: Icon(
                suffixicon,
                color: suffixiconcolor,
              ),
              onPressed: suffixiconfunction,
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
          ),
        ),
      ),
    );
  }
}
