import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class TwoButton extends StatefulWidget {
  @override
  _TwoButtonState createState() => _TwoButtonState();
}

class _TwoButtonState extends State<TwoButton> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Color(0xFF7BD140)),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * .07,
            child: FlatButton(
              onPressed: onButtonPressed,
              child: Text(
                "قبول",
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * .02,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Color(0xFFF54D47)),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * .07,
            child: FlatButton(
              onPressed: () {
              },
              child: Text(
                "ارفض",
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * .03,
        ),
      ],
    );
  }

  void onButtonPressed() {
    showModalBottomSheet(
        backgroundColor: Color(0xFF848484),
        context: context, builder: (context) {
      return Container(
        decoration:BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20))
        ),
        height: MediaQuery.of(context).size.height*.5,
        child: Padding(
          padding: const EdgeInsets.only(top: 25),
          child: Column(
            children: <Widget>[
              Text('الدفع',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
              Container(
                  width:MediaQuery.of(context).size.width*4,
                  height: MediaQuery.of(context).size.height*.2,
                  child: Image.asset('assets/images/accept.png')),
              Text('تمت العملية الدفع بنجاح',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
              SizedBox(height: MediaQuery.of(context).size.height*.03,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Color(0xFF55C2D8)
                  ),
                  width: MediaQuery.of(context).size.width,
                  height:MediaQuery.of(context).size.height*.07,
                  child: FlatButton(
                    onPressed: (){
                    },
                    child: Text("الأنتقال الي الصفحة الرئيسية",style: TextStyle(
                        color: Colors.white,
                        fontSize: 18
                    ),),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}

