import 'package:flutter/material.dart';

class Textfield extends StatelessWidget {
  final String hint;
final TextInputType type;
 final int maxline;
  final int color;


  Textfield({this.hint,this.type,this.maxline=1,this.color=0xFFF3F7FB,});


  @override
  
  Widget build(BuildContext context) {
    
    return Padding(
      padding: const EdgeInsets.only(top:10,bottom: 10,left:20 ,right:20),
      child: Directionality(
        textDirection: TextDirection.rtl,
              child: TextFormField(
   maxLines:maxline,     

      keyboardType: type,
     
          keyboardAppearance: Brightness.dark,
          cursorColor:Color(0xFFF3F7FB),
          decoration: InputDecoration( 
            
          counterText: '',
          
            contentPadding: EdgeInsets.symmetric(vertical: 15,horizontal: 20),
              hintStyle: TextStyle(color: Color(0xFFB4B4B4),fontSize: 13),
              hintText: hint,
          filled: true,
          fillColor: Color(color),  
              enabledBorder: OutlineInputBorder(
                 
                  borderSide: BorderSide(color:Color(0xFFF3F7FB))),
              focusedBorder: OutlineInputBorder(
                 
                  borderSide: BorderSide(color:Color(0xFFF3F7FB))),
                  border: OutlineInputBorder(
                 
                  borderSide: BorderSide(color:Color(0xFFF3F7FB)))
                  ),
                
        ),
      ),

    );}}

   