import 'package:flutter/material.dart';



class UserType extends StatelessWidget {
 final Color color;
 final String type;
 final String img;
  UserType(this.color,this.type,this.img);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 31),
      child: Container(
        height: MediaQuery.of(context).size.height*.1,
        width:MediaQuery.of(context).size.width*.7 ,
decoration: BoxDecoration(
  border: Border.all(color: Color(0xFFDDDDDD)),
  borderRadius: BorderRadius.circular(5)
),
        child: Directionality(textDirection: TextDirection.rtl,
                  child: Row(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 17,bottom: 10,right: 10,left: 90),
              child: CircleAvatar(
                backgroundColor: color,
                radius: 30,
                child: Container(
                  height: MediaQuery.of(context).size.height*.04,
        width:MediaQuery.of(context).size.width*.1 ,
                  child: Image.asset(img)),
              ),
            )
            ,Text(type,style: TextStyle(color: Color(0xFF061209),fontSize: 16,),)
          ],),
        ),
        
      ),
    );
  }
}