import 'package:flutter/material.dart';

class XIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
              padding: const EdgeInsets.only(top: 10,right: 310),
              child: Container(
                
                child: Image.asset('assets/x.png'),
                  height: MediaQuery.of(context).size.height *.04,

                      width: MediaQuery.of(context).size.width *.09,
                      decoration: BoxDecoration(
                         boxShadow: [
        BoxShadow(
          color: Color(0xFFB4B4B4),
         
          blurRadius: 9,
          offset: Offset(0, 3), 
        ),
    ],
                        color: Color(0xFFFFFFFF),borderRadius: BorderRadius.circular(30)),
              ),
            );
  }
}