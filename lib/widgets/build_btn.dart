import 'package:flutter/material.dart';

class BuildButton extends StatelessWidget {
  final String text;
  BuildButton({this.text});
  @override
  Widget build(BuildContext context) {
    return Container(
      height:MediaQuery.of(context).size.height*.04 ,
      width:MediaQuery.of(context).size.width*.4 ,
      decoration: BoxDecoration(
        color: Color(0xFF55C2D8),
        borderRadius: BorderRadius.circular(5),

      ),
      child: Center(child: Text(text,style: TextStyle(color: Color(0xFFFFFFFF)),)),
      
    );
  }
}


