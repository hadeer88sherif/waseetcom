import 'package:flutter/material.dart';


class ComplainItem extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 25,left: 10,top: 10,bottom: 10),
      child: Row(
        children: <Widget>[
        Container(
 width: MediaQuery.of(context).size.width*.01,
          height: MediaQuery.of(context).size.height*.1
,color: Color(0xFFFFB031),
        ),
          Container(
            width: MediaQuery.of(context).size.width*.86,
            height: MediaQuery.of(context).size.height*.1,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft:Radius.circular(10),topLeft:Radius.circular (10) )
              ,border:Border.all(color: Color(0xFFE3E3E3)),
              
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('تصميم 15 صفحة خاصة بالبيتزا')
                  ,Row(
                    children: <Widget>[
                      Text('قيد الدراسة',style: TextStyle(color: Color(0xFFFFB031)),)
                      ,Spacer(),
Text('15-12-2020')
                    ],

                  )
                ],
              ),
            ),
            
          ),
        ],
      ),
    );
  }
}