import 'package:flutter/material.dart';

class NotificationItem extends StatelessWidget {
  final int color;
  final String img;
  final String text;
  final String discription;
  final String date;
  NotificationItem({this.color,this.img,this.text,this.discription,this.date});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
      child: Directionality(
        textDirection: TextDirection.rtl,
            child: Row(
             
             crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
         Container(child: Center(child: Image.asset(img)),
           width: MediaQuery.of(context).size.width*.11,
           height:  MediaQuery.of(context).size.height*.06,
           decoration: BoxDecoration(
             borderRadius:BorderRadius.circular(5),
             color: Color(color) ,
           ),
         ),
         SizedBox(width: MediaQuery.of(context).size.width*.03,),
         Padding(
           padding: const EdgeInsets.only(right: 15),
           child: Container(
             width: MediaQuery.of(context).size.width*.71,
             
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: <Widget>[
                 Text(text,style: TextStyle(color: Color(0xFF55C2D8)),),
                 Text(discription,style:TextStyle(fontSize: 12),overflow:TextOverflow.visible,),
 SizedBox(height: MediaQuery.of(context).size.height*.01,),
  Directionality(
              textDirection: TextDirection.ltr,
              child: Text(date,style: TextStyle(color:Color(0xff010101).withOpacity(.32)),))
           
           ,Divider()
               ],
             ),
           ),
         ),
          
          ],
        ),
      ),
    );
  }
}