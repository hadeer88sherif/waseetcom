import 'package:flutter/material.dart';
class MyOrder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, right: 10, left: 10),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * .1 + 40,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 15,),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text('تصميم 15 صفحة خاصة بالبيتزا',textDirection: TextDirection.rtl,style: TextStyle(
                        fontWeight: FontWeight.w600
                    ),),
                    Text('قيد التنفيذ',style: TextStyle(color: Colors.grey),),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text('21 يوم'),
                            SizedBox(
                              width: MediaQuery.of(context)
                                  .size
                                  .width *
                                  .02,
                            ),
                            Image.asset(
                                'assets/images/calendar.png'),
                          ],
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width *
                              .1,
                        ),
                        Row(
                          children: <Widget>[
                            Text('22150 رس'),
                            SizedBox(
                              width: MediaQuery.of(context)
                                  .size
                                  .width *
                                  .02,
                            ),
                            Image.asset('assets/images/wallet.png'),
                          ],
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width *
                              .1,
                        ),
                        Row(
                          children: <Widget>[
                            Text('19245285'),
                            SizedBox(
                              width: MediaQuery.of(context)
                                  .size
                                  .width *
                                  .02,
                            ),
                            Image.asset('assets/images/name.png'),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              color: Color(0xFFFFB43D),
              width: MediaQuery.of(context).size.width * .01,
              height: MediaQuery.of(context).size.height * .1 + 40,
            ),
          ],
        ),
      ),
    );
  }
}
