

import 'package:flutter/material.dart';

class ReviewItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Container(
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Column(
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal:15, vertical: 15),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Image.asset('assets/bg3.png'),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 12),
                          child: Image.asset('assets/user2.png'),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * .04,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('محمد أحمد',),
                        Text('تصميم 15 صفحة خاصة بالبيتزا',),
                      
                      ],
                    ),
                    Spacer(),
                    Image.asset('assets/halfstar.png'),
                    Image.asset('assets/star.png'),
                    Image.asset('assets/star.png'),
                    Image.asset('assets/star.png'),
                    Image.asset('assets/star.png'),
                  ],
                ),
              ),
            Padding(
              padding: const EdgeInsets.only(right: 75,left: 20),
              child: Text('التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا التقييم هنا',style: TextStyle(fontSize: 12),),
            ),
            
            Padding(
              padding: const EdgeInsets.only(right:250,top: 10),
              child: Text('2018/10/15'),
            )
            ],
          ),
        ),
        decoration: BoxDecoration(
            border: Border.all(color: Color(0xFFE3E3E3)),
            color: Color(0xFFFFFFFF),
            borderRadius: BorderRadius.circular(10)),
        height: MediaQuery.of(context).size.height * .23,
        width: MediaQuery.of(context).size.width,
      ),
    );
  }
}
