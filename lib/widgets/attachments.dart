import 'package:flutter/material.dart';
class Attachments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Color(0xFFF0F0F0),
              borderRadius: BorderRadius.circular(6),
            ),
            width: MediaQuery.of(context).size.width*.2+20,
            child: Image.asset('assets/images/camera.png'),
          ),
          SizedBox(width: MediaQuery.of(context).size.width*.1,),
          Container(
            decoration: BoxDecoration(
              color: Color(0xFFF0F0F0),
              borderRadius: BorderRadius.circular(6),
            ),
            width: MediaQuery.of(context).size.width*.2+20,
            child: Image.asset('assets/images/camera.png'),
          ),
          SizedBox(width: MediaQuery.of(context).size.width*.1,),
          Container(
            decoration: BoxDecoration(
              color: Color(0xFFF0F0F0),
              borderRadius: BorderRadius.circular(6),
            ),
            width: MediaQuery.of(context).size.width*.2+20,
            child: Image.asset('assets/images/camera.png'),
          ),
          SizedBox(width: MediaQuery.of(context).size.width*.1,),
          Container(
            decoration: BoxDecoration(
              color: Color(0xFFF0F0F0),
              borderRadius: BorderRadius.circular(6),
            ),
            width: MediaQuery.of(context).size.width*.2+20,
            child: Image.asset('assets/images/camera.png'),
          ),
        ],
      ),
    );
  }
}
