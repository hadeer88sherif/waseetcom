import 'package:flutter/material.dart';


class BuildContainer extends StatelessWidget {
  final String img;
  final String app;
  BuildContainer({this.img,this.app});
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
            child: Container(
        
        height: MediaQuery.of(context).size.height*.06,
        decoration: BoxDecoration(color: Color(0xFFF5F5F5),
        borderRadius: BorderRadius.circular(30)
        ),
        child: Row(
          children: <Widget>[
               SizedBox(width:MediaQuery.of(context).size.width*.02),
            Image.asset(img),
         
            SizedBox(width: MediaQuery.of(context).size.width*.06,),
            Text(app)
          ],
        ),
        
      ),
    );
  }
}