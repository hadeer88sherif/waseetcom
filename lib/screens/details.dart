import '../widgets/attachments.dart';
import '../widgets/twoButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
enum FilterOptions {
  Contact_administration,
  Report_problem,
}
class Details extends StatefulWidget {
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  bool button = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            Icon(
              Icons.share,
              color: Colors.black,
            ),
            PopupMenuButton(
              onSelected: (FilterOptions selectedValue) {
                setState(() {
                  if (selectedValue == FilterOptions.Contact_administration) {
                    //
                  } else {
                    //
                  }
                });
              },
              icon: Icon(
                Icons.more_vert,
                color: Colors.black,
              ),
              itemBuilder: (_) => [
                PopupMenuItem(
                  child: Text('تواصل مع الأدارة'),
                  value: FilterOptions.Contact_administration,
                ),
                PopupMenuItem(
                  child: Text('تبليغ عن مشكلة'),
                  value: FilterOptions.Report_problem,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10,left: 280),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.black,
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 10, top: 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "تفاصيل العقد",
                      style: TextStyle(
                        fontSize: 23,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, right: 10, left: 10, bottom: 10),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(width: 1, color: Colors.grey),
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 15,
                        ),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                'تصميم 15 صفحة خاصة بالبيتزا',
                                textDirection: TextDirection.rtl,
                                style: TextStyle(fontWeight: FontWeight.w600),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text('21 يوم'),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .02,
                                      ),
                                      Image.asset('assets/images/calendar.png'),
                                    ],
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * .1,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text('22150 رس'),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .02,
                                      ),
                                      Image.asset('assets/images/wallet.png'),
                                    ],
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * .1,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text('19245285'),
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .02,
                                      ),
                                      Image.asset('assets/images/name.png'),
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.fromBorderSide(
                          BorderSide(color: Colors.grey, width: 1)),
                    ),
                    height: MediaQuery.of(context).size.height * .1 + 50,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                '4102215485120',
                                textDirection: TextDirection.ltr,
                                style: TextStyle(fontWeight: FontWeight.w600),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * .3,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    'الطرف الاول',
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.grey),
                                  ),
                                  Text(
                                    'محمد احمد حسام الدين',
                                    textDirection: TextDirection.rtl,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * .01,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                '4102215485120',
                                textDirection: TextDirection.ltr,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * .3,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    'الطرف الثاني',
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.grey),
                                  ),
                                  Text(
                                    'محمد احمد حسام الدين',
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.fromBorderSide(
                        BorderSide(color: Colors.grey, width: 1)),
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .2 + 50,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      'هنا تفاصيل العقد هنا تفاصيل العقد هنا تفاصيل العقد هنا تفاصيل العقد هنا تفاصيل العقد هنا تفاصيل العقد هنا تفاصيل العقد هنا تفاصيل العقدهنا تفاصيل العقد هنا تفاصيل العقدهنا تفاصيل العقد هنا تفاصيل العقدهنا تفاصيل العقد هنا تفاصيل العقدهنا تفاصيل العقد هنا تفاصيل العقد',
                      textDirection: TextDirection.rtl,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      'المرفقات',
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Attachments(),
              SizedBox(
                height: MediaQuery.of(context).size.height * .03,
              ),
              button
                  ? TwoButton()
                  : Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Color(0xFF55C2D8)),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .07,
                        child: FlatButton(
                          onPressed: () {
                            setState(() {
                              button = true;
                            });
                          },
                          child: Text(
                            "الدفع الان",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                    )
            ],
          ),
        ));
  }
}
