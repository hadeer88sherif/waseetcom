import 'dart:async';

import 'package:flutter/material.dart';
import 'intro.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => Intro(),
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF121A4F),
        body: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  height: 170,
                  width: 170,
                  child:  Image.asset('assets/images/topsplash.png'),
                ),
              ],
            ),
            Center(
              child: Container(
                  height: MediaQuery.of(context).size.height*.3,
                  width: MediaQuery.of(context).size.width*.5,
                  child: Image.asset('assets/images/logo.png')),
            ),
            SizedBox(height: MediaQuery.of(context).size.height*.1,),
            Center(
              child: Container(
                  height: MediaQuery.of(context).size.height*.1,
                  width: MediaQuery.of(context).size.width*.5,
                  child: Image.asset('assets/images/circle.png')),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 170,
                        width: 170,
                        child:  Image.asset('assets/images/downsplash.png'),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
