import 'package:flutter/material.dart';
import 'package:waseetcom/screens/reviews.dart';
import 'package:waseetcom/widgets/X_icon.dart';
import 'package:waseetcom/widgets/btn.dart';

import 'package:waseetcom/widgets/textform.dart';



class NewPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         title: Text(
          'استعادة كلمة المرور',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor:Color(0xFFFFFFFF) ,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
           XIcon(),
            SizedBox(height:MediaQuery.of(context).size.height*.05,),
   Textfield(
     type: TextInputType.number,
              hint: 'كلمة المرور',
            ),
              Textfield(
                type: TextInputType.number,
              hint: 'تأكيد كلمة المرور',
            ),  SizedBox(height:MediaQuery.of(context).size.height*.01),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (ctx)=>ReviewScreen()));
              },
                            child: BtnBuilder(text:'حفظ كلمة المرور ')
            ),        
          ],
        ),
      ),
    );
  }
}