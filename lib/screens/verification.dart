import 'package:flutter/material.dart';
import 'package:waseetcom/screens/new_password.dart';
import 'package:waseetcom/widgets/X_icon.dart';
import 'package:waseetcom/widgets/btn.dart';





import 'package:pin_code_fields/pin_code_fields.dart';

class VerifyPassword extends StatefulWidget {
  

  @override
  _VerifyPasswordState createState() => _VerifyPasswordState();
}

class _VerifyPasswordState extends State<VerifyPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         title: Text(
          'استعادة كلمة المرور',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor:Color(0xFFFFFFFF) ,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
          XIcon(),
            SizedBox(height:MediaQuery.of(context).size.height*.05,),


   Padding(
     padding: const EdgeInsets.symmetric(horizontal: 40),
     child: PinCodeTextField(
  length: 4,
  obsecureText: false,
 
  pinTheme: PinTheme(
      shape: PinCodeFieldShape.box,
      borderRadius: BorderRadius.circular(5),
      fieldHeight:MediaQuery.of(context).size.height*.065,
      fieldWidth: MediaQuery.of(context).size.width*.15,
      activeFillColor: Color(0xFFF3F7FB),
      inactiveFillColor: Color(0xFFF3F7FB),
      selectedFillColor: Color(0xFFF3F7FB),
      inactiveColor: Color(0xFFF3F7FB),
      selectedColor: Color(0xFFF3F7FB),
      activeColor: Color(0xFFF3F7FB),
  ),
  animationDuration: Duration(milliseconds: 300),
  
  enableActiveFill: true,
  
  
  onChanged: (value) {
      print(value);
     
  },

),
   ),
              SizedBox(height:MediaQuery.of(context).size.height*.01,),
            InkWell(
              onTap: (){
                Navigator.push(context,MaterialPageRoute(builder: (ctx)=>NewPassword()));
              },
                            child: BtnBuilder(text:' التالى  ')
            ),        
          ],
        ),
      ),
    );
  }
}



