import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderWay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(Icons.arrow_forward_ios,color: Colors.black,),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text("الدفع",style: TextStyle(fontSize: 23,fontWeight: FontWeight.w500),),
              ],
            ),
            Text("من فضلك اختر وسيلة الدفع",style: TextStyle(fontSize: 17),),
            Container(
              decoration: BoxDecoration(
                border:Border.all(width: 1,color: Colors.grey)
              ),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width*.1+15,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text("فيزا"),
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: Image.asset('assets/images/visa.png'),
                  ),
                ],
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height*.02,),
            Container(
              decoration: BoxDecoration(
                  border:Border.all(width: 1,color: Colors.grey)
              ),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width*.1+15,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text("ماستر كارد"),
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: Image.asset('assets/images/mastercard.png'),
                  ),
                ],
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height*.02,),
            Container(
              decoration: BoxDecoration(
                  border:Border.all(width: 1,color: Colors.grey)
              ),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width*.1+15,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text("مدي"),
                  Padding(
                    padding: const EdgeInsets.only(right: 8,left: 5),
                    child: Image.asset('assets/images/mada.png'),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
