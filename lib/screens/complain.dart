import 'package:flutter/material.dart';
import 'package:waseetcom/screens/add_complain.dart';
import 'package:waseetcom/widgets/complain_item.dart';

class CompelainScreen extends StatefulWidget {
  @override
  _CompelainScreenState createState() => _CompelainScreenState();
}

class _CompelainScreenState extends State<CompelainScreen> {
  Widget _buildContainer({String str, int color}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * .27,
            height: MediaQuery.of(context).size.height * .04,
            child: Center(
              child: Text(str),
            ),
            decoration: BoxDecoration(
               borderRadius: BorderRadius.only(topLeft:Radius.circular(5),topRight: Radius.circular(5)),

              border:Border.all(color: Color(0xFFE3E3E3)),
              boxShadow: [
                BoxShadow(
                  color: Color(0xFF0000000D),
                  blurRadius: 9,
                  offset: Offset(0, 3),
                ),
              ],
            ),
          ),
          Container(
             width: MediaQuery.of(context).size.width * .27,
            height:  MediaQuery.of(context).size.height * .002,
            color: Color(color),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Image.asset('assets/arrow.png'),
          ),
          title: Text(
            'الشكاوي',
            style: TextStyle(fontSize: 23, color: Color(0xFF061209)),
          ),
          backgroundColor: Color(0xFFFFFFFF),
          elevation: 0,
        ),
        backgroundColor: Color(0xFFFFFFFF),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
              child: Row(
                children: <Widget>[
                  _buildContainer(color: 0xFF7BD140, str: 'مفتوح')
                  ,_buildContainer(color:0xFFFFB031,str: 'قيد الدراسة' )
                  ,_buildContainer(color: 0xFFFF3A3A,str: 'مغلق')
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 25,top: 10,bottom: 10),
              child: Text('جميع الشكاوي'),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height- MediaQuery.of(context).size.height*.4,
              child: ListView.builder(itemBuilder:(ctx,i)=>ComplainItem(),itemCount: 3,))
              ,Padding(
                padding: const EdgeInsets.only(right: 20),
                child: InkWell(
                  onTap: (){
                    Navigator.push(context,MaterialPageRoute(builder: (ctx)=>AddComplain()));
                  },
                                  child: Container(
                    child: Center(child:Image.asset('assets/add.png') ,),
                    width: MediaQuery.of(context).size.width*.18,
                    height: MediaQuery.of(context).size.height*.09,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      gradient: LinearGradient(colors:[
                        Color(0xFFFF7733),
                        Color(0xFFFF3A3A)
                      ],begin:AlignmentDirectional.centerEnd,end: AlignmentDirectional.bottomEnd),
                      
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
