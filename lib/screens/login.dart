

import 'package:flutter/material.dart';
import 'package:waseetcom/screens/choose_user_type.dart';
import 'package:waseetcom/widgets/btn.dart';
import 'package:waseetcom/widgets/buildContainer.dart';
import 'package:waseetcom/widgets/textform.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Image.asset('assets/arrow.png'),
          )
        ],
        title: Text(
          'تسجيل الدخول',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor: Color(0xFFFFFFFF),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 137, right: 136),
              child: Container(
                height: MediaQuery.of(context).size.height * .3,
                width: MediaQuery.of(context).size.width * .3,
                child: Image.asset('assets/logo.png'),
              ),
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        Image.asset('assets/flag.png'),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        Text('+966')
                      ],
                    ),
                    height: MediaQuery.of(context).size.height * .065,
                    width: MediaQuery.of(context).size.width * .2,
                    decoration: BoxDecoration(color: Color(0xFFF3F7FB)),
                  ),
                ),
                Expanded(
                  child: Textfield(
                    hint: 'رقم الجوال',
                  ),
                ),
              ],
            ),
            Textfield(
              hint: 'كلمة المرور',
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 130,vertical: 20),
              child: Text('استعادة كلمة المرور'),
            ),
           BtnBuilder(text: 'تسجيل الدخول',),
            Padding(
              padding: const EdgeInsets.only(top: 20,left: 40,right: 40,bottom: 30),
              child: Row(
                children: <Widget>[
                  Expanded(child: BuildContainer(img: 'assets/google.png',app: 'جوجل',))
                  ,SizedBox(width:MediaQuery.of(context).size.width*.06,)
                  ,Expanded(child: BuildContainer(img: 'assets/facebook.png',app: 'فيسبوك',))
                ],
              ),
            ),
            Row(children: <Widget>[
              SizedBox(width:MediaQuery.of(context).size.width*.3,),
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (ctx)=>ChooseUserType()));
                },
                child: Text('سجل الان',style: TextStyle(color: Color(0xFF55C2D8),fontSize: 15),))
                ,SizedBox(width:MediaQuery.of(context).size.width*.04,),
              Text('ليس لديك حساب ؟',style: TextStyle(fontSize: 15),),
              
            ],)
          ],
        ),
      ),
    );
  }
}
