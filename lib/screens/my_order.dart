import '../widgets/my_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Myorder extends StatefulWidget {
  @override
  _MyorderState createState() => _MyorderState();
}

enum Gender { waiting, complete, underway }

class _MyorderState extends State<Myorder> {
  Gender gender;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: Stack(
              alignment: Alignment.bottomLeft,
              overflow: Overflow.visible,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .9,
                  child: Image.asset('assets/images/myorder.png'),
                ),
                Positioned(
                  bottom: -20,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              gender = Gender.complete;
                            });
                          },
                          child: Cart(
                            color: Color(0xFF7BD140),
                            text: 'المكتملة',
                            cartColor: gender == Gender.complete
                                ? Color(0xFF7BD140)
                                : Color(0xFFFFFFFF),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              gender = Gender.waiting;
                            });
                          },
                          child: Cart(
                            color: Color(0xFFFFB43D),
                            text: 'قيد التنفيذ',
                            cartColor: gender == Gender.waiting
                                ? Color(0xFFFFB43D)
                                : Color(0xFFFFFFFF),
                          ),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              gender = Gender.underway;
                            });
                          },
                          child: Cart(
                            color: Color(0xFF55C2D8),
                            text: 'تحت الأنتظار',
                            cartColor: gender == Gender.underway
                                ? Color(0xFF55C2D8)
                                : Color(0xFFFFFFFF),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: ListView(
              children: <Widget>[
                MyOrder(),
                MyOrder(),
                MyOrder(),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class Cart extends StatelessWidget {
  Cart({this.color, this.text, this.cartColor});

  final String text;
  final Color color;
  final Color cartColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: cartColor,
        borderRadius: BorderRadius.circular(6),
      ),
      height: MediaQuery.of(context).size.height * .1,
      width: MediaQuery.of(context).size.width * .3,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(text),
          ),
          Text(
            'عقود 3',
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
          Divider(
            color: color,
            endIndent: 0,
            indent: 0,
            thickness: 5,
          )
        ],
      ),
    );
  }
}
