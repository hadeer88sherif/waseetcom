import 'package:flutter/material.dart';
import 'package:waseetcom/widgets/btn.dart';
import 'package:waseetcom/widgets/textform.dart';



class AddComplain extends StatelessWidget {
  void onButtonPressed(BuildContext context) {
    showModalBottomSheet(
        backgroundColor: Color(0xFF848484).withOpacity(.50),
        context: context, builder: (context) {
      return Container(
        decoration:BoxDecoration(
            color: Colors.white,
          borderRadius: BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20))
        ),
        height: MediaQuery.of(context).size.height*.6,
        child: Padding(
          padding: const EdgeInsets.only(top: 25),
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width*.14,
                height: MediaQuery.of(context).size.height*.003,
                color: Color(0xFFE8E8E8),
              ),
              SizedBox(height: MediaQuery.of(context).size.height*.02,),
              Text(' اضافة شكوي',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
              Image.asset('assets/bg.png'),
              SizedBox(height: MediaQuery.of(context).size.height*.02,),
              Text('تم ارسال الشكوي بنجاح',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w300),),
              SizedBox(height: MediaQuery.of(context).size.height*.02,),
               BtnBuilder(text:' الانتقال الي الصفحة الرئيسية'),
             
            ],
          ),
        ),
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
         appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Image.asset('assets/arrow.png'),
          )
        ],
        title: Text(
          'اضافة شكوي جديدة ',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor: Color(0xFFFFFFFF),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: <Widget>[
            
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Textfield(
                hint: 'الاسم',
            ),
              ),
              Textfield(
                hint: 'تفاصيل الشكوي',
                maxline: 10,
            ),

               Directionality(
        textDirection: TextDirection.rtl,
              child:
          Row(
            children: <Widget>[
                 SizedBox(width:MediaQuery.of(context).size.width*.05),
                  Container(
                    child:  Image.asset('assets/camera.png'),
           
          
          height: MediaQuery.of(context).size.height*.06,
          decoration: BoxDecoration(color: Color(0xFFF5F5F5),
          borderRadius: BorderRadius.circular(5)
          ),),
           SizedBox(width:MediaQuery.of(context).size.width*.02)
             
             ,
              Text('اضافة صور للتوضيح')
            ],
          ),
          
        
    ), SizedBox(height:MediaQuery.of(context).size.height*.01,),
                 InkWell(
                   onTap: (){
                     onButtonPressed(context);
                   },
                                    child: BtnBuilder(text:' ارســال'),
                 ),
          ],
        ),
      ),
    );
  }
}