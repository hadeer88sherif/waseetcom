import '../screens/order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Hesapy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFE3E3E3),
        appBar: AppBar(
          elevation: 0,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.black,
              ),
            )
          ],
          backgroundColor: Colors.white,
          title: Center(
              child: Text(
            "حسابي",
            style: TextStyle(color: Colors.black),
          )),
        ),
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  alignment: Alignment.bottomLeft,
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xFFFFFFFF),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: 130,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 30),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'محمد احمد حسام الدين',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    '+966 465548851',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 20),
                              child: Container(
                                  height: 70,
                                  width: 100,
                                  child:
                                      Image.asset("assets/images/hesapy.png")),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: -10,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 15, right: 10),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFFFFD6D6),
                                borderRadius: BorderRadius.circular(6),
                              ),
                              height: 40,
                              width:
                                  MediaQuery.of(context).size.width * .4 + 20,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('12365',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 12,
                                          color: Color(0xFFFF6360)),
                                      textDirection: TextDirection.ltr),
                                  Text(
                                    "الرصيد المحجوز  رس ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                        color: Color(0xFFFF6360)),
                                    textDirection: TextDirection.rtl,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFFCDFAD8),
                                borderRadius: BorderRadius.circular(6),
                              ),
                              height: 40,
                              width:
                                  MediaQuery.of(context).size.width * .4 + 20,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('12365',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 12,
                                          color: Color(0xFF33A74E)),
                                      textDirection: TextDirection.ltr),
                                  Text(
                                    "الرصيد المتاح  رس ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                        color: Color(0xFF33A74E)),
                                    textDirection: TextDirection.rtl,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 25, left: 10, right: 10),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 280,
                        decoration: BoxDecoration(
                          color: Color(0xFFFFFFFF),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Column(
                          children: <Widget>[
                            Setting(
                              color: Color(0xFFFFD6D6),
                              name: "تعديل الحساب",
                              image: 'assets/images/setting.png',
                              textColor: Colors.black,
                              fontSize: 14,
                            ),
                            DividerS(),
                            Setting(
                              color: Color(0xFFCDFAD8),
                              name: "التقيمات",
                              image: 'assets/images/favorite.png',
                              textColor: Colors.black,
                              fontSize: 14,
                            ),
                            DividerS(),
                            Setting(
                              color: Color(0xFFD9F8FF),
                              name: "الشكاوي",
                              image: 'assets/images/send.png',
                              textColor: Colors.black,
                              fontSize: 14,
                            ),
                            DividerS(),
                            Setting(
                              color: Color(0xFFFAE0CD),
                              name: "الأسئله الشائعة",
                              image: 'assets/images/Path.png',
                              textColor: Colors.black,
                              fontSize: 14,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: Container(
                    height: 210,
                    decoration: BoxDecoration(
                      color: Color(0xFFFFFFFF),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Column(
                      children: <Widget>[
                        Setting(
                          color: Color(0xFFCCDEF8),
                          name: "تواصل معانا",
                          image: 'assets/images/support.png',
                          textColor: Colors.black,
                          fontSize: 14,
                        ),
                        DividerS(),
                        Setting(
                          color: Color(0xFFF4F4F4),
                          name: "عن التطبيق",
                          image: 'assets/images/Group.png',
                          textColor: Colors.black,
                          fontSize: 14,
                        ),
                        DividerS(),
                        Setting(
                          color: Color(0xFFB8ECF7),
                          name: "شارك التطبيق",
                          image: 'assets/images/share.png',
                          textColor: Colors.black,
                          fontSize: 14,
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Container(
                    child: FlatButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return Order();
                        }));
                      },
                      child: Text(
                        "تسجيل الخروج",
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}

class DividerS extends StatelessWidget {
  const DividerS({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Color(0xFFD5D5D5),
      endIndent: 20,
      indent: 20,
      thickness: 1,
    );
  }
}

class Setting extends StatelessWidget {
  Setting({this.color, this.image, this.name, this.textColor, this.fontSize});

  final String image;
  final Color color;
  final String name;
  final Color textColor;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, left: 10, right: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              name,
              style: TextStyle(
                  fontSize: fontSize,
                  fontWeight: FontWeight.w600,
                  color: textColor),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(6),
            ),
            width: 50,
            height: 50,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(child: Image.asset(image)),
            ),
          ),
        ],
      ),
    );
  }
}
