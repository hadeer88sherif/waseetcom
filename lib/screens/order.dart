import '../widgets/textFormField.dart';

import 'package:flutter/material.dart';

import 'New_contract_request.dart';
class Order extends StatefulWidget {
  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(Icons.arrow_forward_ios,color: Colors.black,),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 30,bottom:5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text('الدفع',style: TextStyle(fontSize: 23,fontWeight: FontWeight.w600),),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 40,bottom:5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text('أضف بيانات الكارت',style: TextStyle(fontSize: 17),),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: TextForm(
              maxLine: 3,
              hinttext: "الأسم ",
              height: MediaQuery.of(context).size.height*.07,
              width: MediaQuery.of(context).size.width,
              color: Color(0xFFF3F7FB),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height*.02,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: TextForm(
              maxLine: 3,
              hinttext: "رقم الكارت",
              height: MediaQuery.of(context).size.height*.07,
              width: MediaQuery.of(context).size.width,
              color: Color(0xFFF3F7FB),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height*.02,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: TextForm(
              maxLine: 3,
              hinttext: "تاريخ الأنتهاء",
              height: MediaQuery.of(context).size.height*.07,
              width: MediaQuery.of(context).size.width,
              color: Color(0xFFF3F7FB),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height*.02,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: TextForm(
              maxLine: 3,
              hinttext: "CW",
              height: MediaQuery.of(context).size.height*.07,
              width: MediaQuery.of(context).size.width,
              color: Color(0xFFF3F7FB),
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height*.02,),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Color(0xFF55C2D8)
              ),
              width: MediaQuery.of(context).size.width,
              height:MediaQuery.of(context).size.height*.07,
              child: FlatButton(
                onPressed: onButtonPressed,
                child: Text("الدفع الان",style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                ),),
              ),
            ),
          )
        ],
      ),
    );
  }
  void onButtonPressed() {
    showModalBottomSheet(
        backgroundColor: Color(0xFF848484),
        context: context, builder: (context) {
      return Container(
        decoration:BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20))
        ),
        height: MediaQuery.of(context).size.height*.5,
        child: Padding(
          padding: const EdgeInsets.only(top: 25),
          child: Column(
            children: <Widget>[
              Text('الدفع',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
              Container(
                  width:MediaQuery.of(context).size.width*4,
                  height: MediaQuery.of(context).size.height*.2,
                  child: Image.asset('assets/images/accept.png')),
              Text('تمت العملية الدفع بنجاح',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w600),),
              SizedBox(height: MediaQuery.of(context).size.height*.03,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Color(0xFF55C2D8)
                  ),
                  width: MediaQuery.of(context).size.width,
                  height:MediaQuery.of(context).size.height*.07,
                  child: FlatButton(
                    onPressed: (){
                      Navigator.push(context,MaterialPageRoute(builder: (context){
                        return NewRontractRequest();
                      }));
                    },
                    child: Text("الأنتقال الي الصفحة الرئيسية",style: TextStyle(
                        color: Colors.white,
                        fontSize: 18
                    ),),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
