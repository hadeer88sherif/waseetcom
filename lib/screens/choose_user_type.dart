import 'package:flutter/material.dart';
import 'package:waseetcom/screens/Service_provider_login.dart';
import 'package:waseetcom/screens/loginpostulant.dart';
import 'package:waseetcom/widgets/logo.dart';
import 'package:waseetcom/widgets/user_type.dart';


class ChooseUserType extends StatefulWidget {
  @override
  _ChooseUserTypeState createState() => _ChooseUserTypeState();
}

class _ChooseUserTypeState extends State<ChooseUserType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
      
        
        children: <Widget>[
         Logo(),
          Column(
            children: <Widget>[
              Text('من فضلك اختر نوع المستخدم',style: TextStyle(color: Color(0xFF061209),fontSize: 16,),),
               SizedBox(height: MediaQuery.of(context).size.height*.01,),
               Text('choose user type',style: TextStyle(color: Color(0xFF061209),fontSize: 16,),
               )],
          ),
         
          Padding(
            padding: const EdgeInsets.only(top: 47,bottom: 11),
            child: InkWell(
              onTap: (){
              Navigator.push(context,MaterialPageRoute(builder: (ctx)=>ServiceProviderLoginScreen()));  
              },
              child: UserType(Color(0xFF55C2D8).withOpacity(.13), 'مقدم خدمة', 'assets/desert.png')),
          ),

          InkWell (
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (ctx)=>LoginPostulantScreen()));
            },
            child: UserType(Color(0xFF3F7BF7).withOpacity(.13), 'طالب خدمة', 'assets/rich.png'))
        ],
      ),
    );
  }
}