import 'package:flutter/material.dart';
import 'package:waseetcom/screens/complain.dart';
import 'package:waseetcom/widgets/btn.dart';
import 'package:waseetcom/widgets/textform.dart';

class EditAccountScreen extends StatelessWidget {
  Widget star() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: Image.asset('assets/starempty.png'),
    );
  }

 

  void _onButtonPressed(BuildContext context) {
    showModalBottomSheet(
isScrollControlled: true,
        backgroundColor: Color(0xFF848484).withOpacity(.50),
        context: context,
        builder: (context) {
          return
     DraggableScrollableSheet(
            maxChildSize:.9,
            builder: (BuildContext context,ScrollController scrollController){
                  return    Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20))),
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.only(top: 25),
                child: ListView(
                  controller:scrollController,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * .14,
                          height: MediaQuery.of(context).size.height * .003,
                          color: Color(0xFFE8E8E8),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .01,
                        ),
                        Text(
                          ' اضافة تقييم',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w600),
                        ),
                        Image.asset('assets/user.png'),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .01,
                        ),
                        Text(
                          'محمد أحمد حسام الدين',
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .002,
                        ),
                        Text(
                          'تصميم 15 صفحة خاصة بالبيتزا',
                          style: TextStyle(color: Color(0xFF55C2D8)),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 50, vertical: 20),
                          child: Row(
                            children: <Widget>[
                              star(),
                              star(),
                              star(),
                              star(),
                              star()
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 30),
                          child: Text('اكتب تعليقك علي التقييم'),
                        ),
                        Textfield(
                          hint: 'النص هنا',
                          maxline: 8,
                          color: 0xFFFFFFFF,
                        )
                      ],
                    ),
                     SizedBox(height:MediaQuery.of(context).size.height*.01,),
                    BtnBuilder(text:
                        'اضافة تقييم'),
                  ],
                ),
              ),
          );}
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Image.asset('assets/arrow.png'),
          )
        ],
        title: Text(
          'تعديل الحساب',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor: Color(0xFFFFFFFF),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 140, top: 20, bottom: 15),
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (ctx) => CompelainScreen()));
                  },
                  child: Stack(
                    children: <Widget>[
                      Image.asset('assets/user.png'),
                      Image.asset('assets/foreground.png'),
                      Padding(
                        padding: const EdgeInsets.only(right: 20, top: 35),
                        child: Image.asset('assets/cameraa.png'),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 135),
                child: Text('محمد أحمد حسام الدين'),
              ),
              buildText('الاسم'),
              Textfield(
                hint: 'الاسم',
              ),
              buildText('رقم الجوال'),
              Textfield(
                hint: 'رقم الجوال',
              ),
              buildText('كلمة المرور'),
              Textfield(
                hint: 'كلمة المرور',
              ),
              buildText('اعادة كلمة المرور'),
              Textfield(
                hint: 'اعادة كلمة المرور ',
              ),
                  SizedBox(height:MediaQuery.of(context).size.height*.01,),
    
              InkWell(
                onTap: () {
                 _onButtonPressed(context);
  
                },
                child: BtnBuilder(text:
                    'حفظ التعديلات ',
                    ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildText(String str) {
    return Padding(
      padding: const EdgeInsets.only(right: 25),
      child: Text(str),
    );
  }
}
