import 'package:flutter/material.dart';

import 'hesapy.dart';
class CommonQuestion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Image.asset("assets/images/arrow.png"),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15,top: 10,bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text('الأسئلة الشائعة',style: TextStyle(fontWeight: FontWeight.w600,fontSize: 23),),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني إنشاء عقد جديد ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني عرض تفاصيل الفاتورة ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني إنشاء عقد جديد ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني عرض تفاصيل الفاتورة ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني إنشاء عقد جديد ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني عرض تفاصيل الفاتورة ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني إنشاء عقد جديد ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_back_ios),
                    Setting(
                      color: Color(0xFFFAE0CD),
                      name: "كيف يمكنني عرض تفاصيل الفاتورة ؟",
                      image: 'assets/images/Path.png',
                      textColor: Color(0xFF672B0A),
                      fontSize: 18,
                    ),
                  ],
                ),
              ),
              DividerS(),
            ],
          ),
        ),
      ),
    );
  }
}
