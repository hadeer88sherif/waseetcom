import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Message extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.black,
              ),
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text("الرسائل",style: TextStyle(
                    fontSize: 23,
                  ),),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 10,),
                                child: Text(
                                  "عمرو الشبه",
                                  style: TextStyle(fontWeight: FontWeight.w600),
                                  textDirection: TextDirection.rtl,
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Container(
                                width: MediaQuery.of(context).size.width *.6,
                                child: Text(
                                  "رساله هنا رساله هنا رساله هنا رساله هنا رساله هنا رساله هنا رساله هنا رساله هنا رساله هنا",
                                  style: TextStyle(color: Colors.grey),
                                  textDirection: TextDirection.rtl,
                                )),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          CircleAvatar(
                            child: Image.asset('assets/images/message.png'),
                            radius: 30,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
