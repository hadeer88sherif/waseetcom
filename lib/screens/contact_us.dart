import 'package:flutter/material.dart';
import 'package:waseetcom/widgets/btn.dart';
import 'package:waseetcom/widgets/textform.dart';

class ContactUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Image.asset('assets/arrow.png'),
          )
        ],
        title: Text(
          'تواصل معانا',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor: Color(0xFFFFFFFF),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: <Widget>[
             Padding(
                padding: const EdgeInsets.only(left: 137, right: 136),
                child: Container(
                  height: MediaQuery.of(context).size.height * .15,
                  width: MediaQuery.of(context).size.width * .3,
                  child: Image.asset('assets/contactus.png'),
                ),
              ),
              Textfield(hint: 'الاسم',),
              Textfield(hint: 'رقم الجوال',)
              ,Textfield(hint: 'هنا نص الرسالة',maxline: 10,),
              BtnBuilder(text: 'ارســال',)
          ],
        ),
      ),
    );
  }
}