import 'package:flutter/material.dart';
import 'package:waseetcom/widgets/notification_item.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  var connection=true;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
          child: Scaffold(
          appBar: AppBar(
            leading: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Image.asset('assets/arrow.png'),
            ),
          
          title: Text(
            'الإشعارات',
            style: TextStyle(fontSize: 20, color: Color(0xFF061209)),
          ),
         
          backgroundColor: Color(0xFFFFFFFF),
          elevation: 0,
        ),
        backgroundColor: Color(0xFFFFFFFF),
        body:connection? ListView(
          children: <Widget>[
            NotificationItem(date: '4:32 PM 25\5\2020',color: 0xFFB8ECF7,img: 'assets/neworder.png',text: 'عقد جديد',discription: 'هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات',)
             ,NotificationItem(date: '4:32 PM 25\5\2020',color: 0xFFFAECDF,img: 'assets/newcomplain.png',text: 'شكوي جديدة',discription: 'هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات',)
              ,NotificationItem(date: '4:32 PM 25\5\2020',color: 0xFFDFF5EB,img: 'assets/verified.png',text: 'قبول عقد جديد',discription: 'هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات',),
              NotificationItem(date: '4:32 PM 25\5\2020',color: 0xFFFFA8A8,img: 'assets/rejected.png',text: 'رفض عقد',discription: 'هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات',),
      NotificationItem(date: '4:32 PM 25\5\2020',color: 0xFFB8ECF7,img: 'assets/neworder.png',text: 'عقد جديد',discription: 'هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات هنا نص الاشعارات',)

          ],
        ):Padding(
          padding: const EdgeInsets.symmetric(horizontal: 80,vertical: 80),
          child: Column(
            
            
            children: <Widget>[
              Image.asset('assets/notifications.png')
             , SizedBox(height: MediaQuery.of(context).size.height*.03,)
              ,Text('لايوجد إشعارات'),
               SizedBox(height: MediaQuery.of(context).size.height*.03,),
              Container(
                width: MediaQuery.of(context).size.width*.33,
                height: MediaQuery.of(context).size.height*.042,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Color(0xFF55C2D8))
                ),
                
                child: Row(
                  
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                 
                  Text('اعادة المحاولة',style: TextStyle(color: Color(0xFF55C2D8)),)
                  , Image.asset('assets/return.png'),
                ],),
              )
            ],
          ),
        )
      ),
    );
  }
}