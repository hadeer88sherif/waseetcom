

import 'package:flutter/material.dart';
import 'package:waseetcom/screens/restorepassword.dart';
import 'package:waseetcom/widgets/btn.dart';

import 'package:waseetcom/widgets/textform.dart';

class LoginPostulantScreen extends StatefulWidget {
  @override
  _LoginPostulantScreenState createState() => _LoginPostulantScreenState();
}

class _LoginPostulantScreenState extends State<LoginPostulantScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Image.asset('assets/arrow.png'),
          )
        ],
        title: Text(
          'التسجيل كطالب خدمة',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor: Color(0xFFFFFFFF),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 137, right: 136),
              child: Container(
                height: MediaQuery.of(context).size.height * .15,
                width: MediaQuery.of(context).size.width * .3,
                child: Image.asset('assets/logo.png'),
              ),
            ),
                  Textfield(
              hint: 'الاسم',
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        Image.asset('assets/flag.png'),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        Text('+966')
                      ],
                    ),
                    height: MediaQuery.of(context).size.height * .065,
                    width: MediaQuery.of(context).size.width * .2,
                    decoration: BoxDecoration(color: Color(0xFFF3F7FB)),
                  ),
                ),
               
                Expanded(
                  child: Textfield(
                    hint: 'رقم الجوال',
                  ),
                ),
              ],
            ),
            Textfield(
              hint: 'كلمة المرور',
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30,),
              child:   Row(children: <Widget>[
              SizedBox(width:MediaQuery.of(context).size.width*.14),

              Text('الشروط و الاحكام',style: TextStyle(color: Color(0xFF55C2D8),fontSize: 15),)
                ,SizedBox(width:MediaQuery.of(context).size.width*.04,),
              Text('الموافقة على الشروط و الاحكام',style: TextStyle(fontSize: 15),),
              
            ],),
            ),
            InkWell(
              onTap: (){Navigator.push(context,MaterialPageRoute(builder: (ctx)=>ResetPassword()));},
                            child: BtnBuilder(text:'تسجيل الدخول'),
            ),
            
            Row(children: <Widget>[
              SizedBox(width:MediaQuery.of(context).size.width*.3,),
              Text('سجل الان',style: TextStyle(color: Color(0xFF55C2D8),fontSize: 15),)
                ,SizedBox(width:MediaQuery.of(context).size.width*.04,),
              Text('ليس لديك حساب ؟',style: TextStyle(fontSize: 15),),
              
            ],)
          ],
        ),
      ),
    );
  }
}
