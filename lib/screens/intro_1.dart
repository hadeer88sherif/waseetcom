import 'package:flutter/material.dart';
class Intro1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 100),
            child: Container(
              child: Image.asset('assets/images/wasset.png'),
            ),
          ),
          SizedBox(height: 30,),
          Text("سهولة التواصل في اي وقت",style: TextStyle(
            fontSize: 20,
            color: Colors.white,
            fontFamily: 'URW',
            fontWeight: FontWeight.bold
          ),textAlign: TextAlign.center,),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: new Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.4-40,
              child: Image.asset('assets/images/intro1.png'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 50,left: 50,top: 25),
            child: Container(
              child: Text('داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا داتا هنا ',
              style: TextStyle(
                fontSize: 14.5,
                fontWeight: FontWeight.w600,
                color: Colors.white
              ),),
            ),
          )
        ],
      ),
    );
  }
}
