import 'package:flutter/material.dart';
import 'package:waseetcom/screens/edit.dart';
import 'package:waseetcom/widgets/build_btn.dart';
import 'package:waseetcom/widgets/review_item.dart';

class ReviewScreen extends StatefulWidget {
  @override
  _ReviewScreenState createState() => _ReviewScreenState();
}

class _ReviewScreenState extends State<ReviewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
 appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Image.asset('assets/arrow.png'),
          )
        ],
        title: Text(
          'تقييمات مقدم الخدمة',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
    
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 20),
        child: Column(children: <Widget>[
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: Color(0xFFE3E3E3)),
              color: Color(0xFFFFFFFF),borderRadius: BorderRadius.circular(10)
            ),
            height: MediaQuery.of(context).size.height*.23,
            width: MediaQuery.of(context).size.width,
            child: Directionality(
              textDirection: TextDirection.rtl,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 20,top: 15,bottom: 10),
                          child: Column(
                children: <Widget>[
                  Row(children: <Widget>[
                      Image.asset('assets/user.png'),
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('محمد أحمد حسام الدين')
                              ,Directionality(
                                textDirection: TextDirection.ltr,
                                child: Text('+966 554837738',))
                            ],
                        ),
                      )
                    ],),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(children: <Widget>[
InkWell(
  onTap: (){
    Navigator.push(context, MaterialPageRoute(builder: (ctx)=>EditAccountScreen()));
  },
  child: BuildButton(text:'اجمالي المبلغ ر.س 12738' ,)),
SizedBox(width: MediaQuery.of(context).size.width*.02,),
BuildButton(text: 'عدد الطلبات 15',)
                    ],),
                  )
                ],
              ),
                        ),
            ),
            
          ),Padding(
            padding: const EdgeInsets.only(top: 10),
            child: SizedBox(
              height: MediaQuery.of(context).size.height-MediaQuery.of(context).size.height*.4,
              child: ListView.builder(itemBuilder:(ctx,i)=>ReviewItem(),itemCount: 10,)),
          )
        ],),
      ),

    );
  }
}