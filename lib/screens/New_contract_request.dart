import '../screens/common_question.dart';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../widgets/textFormField.dart';

class NewRontractRequest extends StatefulWidget {
  @override
  _NewRontractRequestState createState() => _NewRontractRequestState();
}

class _NewRontractRequestState extends State<NewRontractRequest> {
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              Icons.arrow_forward_ios,
              color: Colors.black,
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Form(
        key: _globalKey,
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'طلب عقد جديد',
                        style: TextStyle(
                            fontSize: 23, fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 35),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text('مقدم خدمه'),
                      Text(' ID'),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: TextForm(
                    
                    maxLine: 3,
                    hinttext: "#1565984",
                    height: MediaQuery.of(context).size.height * .07,
                    width: MediaQuery.of(context).size.width,
                    color: Color(0xFFF3F7FB),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 35),
                      child: Text('وصف الخدمة'),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: TextForm(
                    hinttext: "وصف الخدمة",
                    maxLine: 15,
                    height: MediaQuery.of(context).size.height * .2,
                    width: MediaQuery.of(context).size.width,
                    color: Color(0xFFF3F7FB),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .02,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 70),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('مدة الأتفاق'),
                      Text('مبلغ الأتفاق'),
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 25, right: 25),
                      child: TextForm(
                        color: Color(0xFFF3F7FB),
                        width: MediaQuery.of(context).size.width * .4,
                        hinttext: "مبلغ الاتفاق",
                        maxLine: 1,
                      ),
                    ),
                    TextForm(
                      color: Color(0xFFF3F7FB),
                      width: MediaQuery.of(context).size.width * .4,
                      hinttext: "مده الأتفاق",
                      maxLine: 1,
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .02,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          "اضافة صوره للتوضيح",
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0xFFF3F7FB),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        width: MediaQuery.of(context).size.width * .2,
                        height: MediaQuery.of(context).size.height * .07,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child:
                                  Image.asset('assets/images/photograph.png')),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .02,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: Color(0xFF55C2D8)),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * .07,
                    child: FlatButton(
                      onPressed: onButtonPressed,
                      child: Text(
                        "انشاء عقد",
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  void onButtonPressed() {
    showModalBottomSheet(
        backgroundColor: Color(0xFF848484),
        context: context,
        builder: (context) {
          return Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20))),
            height: MediaQuery.of(context).size.height * .5,
            child: Padding(
              padding: const EdgeInsets.only(top: 25),
              child: Center(
                child: Column(
                  children: <Widget>[
                    Text(
                      'إنشاء عقد ',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 4,
                        height: MediaQuery.of(context).size.height * .2,
                        child: Image.asset('assets/images/accept.png')),
                    Text(
                      'تمت العملية بنجاح',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .04,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Color(0xFF55C2D8)),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .07,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return CommonQuestion();
                            }));
                          },
                          child: Text(
                            "الأنتقال الي الصفحة الرئيسية",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
