import 'package:flutter/material.dart';
import 'package:waseetcom/screens/verification.dart';
import 'package:waseetcom/widgets/X_icon.dart';
import 'package:waseetcom/widgets/btn.dart';
import 'package:waseetcom/widgets/textform.dart';



class ResetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         title: Text(
          'استعادة كلمة المرور',
          style: TextStyle(fontSize: 15, color: Color(0xFF061209)),
        ),
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        elevation: 0,
      ),
      backgroundColor:Color(0xFFFFFFFF) ,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
        XIcon(),
            SizedBox(height:MediaQuery.of(context).size.height*.05,),
             Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        Image.asset('assets/flag.png'),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * .02,
                        ),
                        Text('+966')
                      ],
                    ),
                    height: MediaQuery.of(context).size.height * .065,
                    width: MediaQuery.of(context).size.width * .2,
                    decoration: BoxDecoration(color: Color(0xFFF3F7FB)),
                  ),
                ),
               
                Expanded(
                  child: Textfield(
                    type: TextInputType.number,
                    hint: 'رقم الجوال',
                  ),
                ),
              ],
            ),
             SizedBox(height:MediaQuery.of(context).size.height*.01,),
             InkWell(
               onTap: (){
                 Navigator.push(context,MaterialPageRoute(builder: (ctx)=>VerifyPassword()));
               },
                             child: BtnBuilder(text:'استعادة كلمة المرور')
             ),
          ],
        ),
      ),
    );
  }
}